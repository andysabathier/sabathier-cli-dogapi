# Dog-CLI-NXP-Sabathier

Dog-CLI-NXP-Sabathier est un outil en ligne de commande pour interagir avec l'API de dog.ceo et obtenir des informations sur les races de chiens.

## Prérequis

Avant d'installer Dog-CLI-NXP-Sabathier, vous devez avoir Python installé sur votre machine.

### Installation de Python

#### Sur Windows :

1. Téléchargez la dernière version de Python depuis le site officiel : [https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows/).
2. Exécutez le fichier d'installation téléchargé et assurez-vous de cocher l'option `Add Python to PATH` au début de l'installation.
3. Suivez les instructions pour terminer l'installation.

#### Sur Linux :

La plupart des distributions Linux viennent avec Python préinstallé. Pour vérifier si Python est installé, ouvrez votre terminal et tapez :

```bash
python --version
```

ou pour Python3 :

```bash
python3 --version
```

Si Python n'est pas installé, vous pouvez l'installer via le gestionnaire de paquets de votre distribution. Par exemple, sur Ubuntu, vous pouvez utiliser `apt` :

```bash
sudo apt update
sudo apt install python3
```

## Installation de Dog-CLI-NXP-Sabathier

### Configuration sur Windows

Sur Windows, vous devrez peut-être lancer l'invite de commande en tant qu'administrateur pour éviter les problèmes de permission lors de l'installation des packages Python. Faites un clic droit sur `Command Prompt` et sélectionnez `Run as administrator`.

Pour installer Dog-CLI-NXP-Sabathier, ouvrez votre terminal ou invite de commande et tapez :

```bash
pip install dog-cli-NXP-Sabathier
```

### Configuration sur Linux

Pour installer Dog-CLI-NXP-Sabathier, ouvrez votre terminal ou invite de commande et tapez :

```bash
pip install dog-cli-NXP-Sabathier
```

Sur certains systèmes Linux, le répertoire où `pip` installe les scripts peut ne pas être inclus dans votre `PATH`. Pour l'ajouter temporairement, exécutez :

```bash
export PATH=$PATH:~/.local/bin
```

Pour rendre cette modification permanente, ajoutez-la à votre fichier `.bashrc` ou `.profile`.
```bash
source ~/.bashrc
```

## Utilisation

Après l'installation, vous pouvez exécuter la commande `dog-breeds list-all` pour interagir avec l'API et obtenir la liste des races de chiens.

```bash
dog-breeds list-all
```

Vous pouvez aussi téléchargé l'image d'une race en particulier avec la commande `dog-breeds get-image` :

```bash
dog-breeds get-image --breed "Boxer" --file "./image.jpg"
```

## Support

Si vous rencontrez des problèmes lors de l'utilisation de Dog-CLI-NXP-Sabathier, veuillez ouvrir un issue sur le dépôt GitLab du projet.

---
